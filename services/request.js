import axios from "axios"

const baseUrl = 'https://api.themoviedb.org/3/'

const instance = axios.create({
    baseURL: baseUrl,
})

instance.interceptors.response.use(
    error => error,
    error => Promise.reject(error)
);

instance.interceptors.request.use((config) => {
    const url = config.url
    config.url = url + '?api_key=6d2b70e5aa8de51dbac14cd7aafe24a6'

    return config;
});

export const getFetcher = (...args) =>
    instance.get(...args).then((res) => res.data);

export const postFetcher = (...args) => instance.post(...args);

export const putFetcher = (...args) => instance.put(...args);

export const patchFetcher = (...args) => instance.patch(...args);

export const deleteFetcher = (...args) => instance.delete(...args)

export const get = (url, params = {}) =>
    instance.get(url, {
        params,
    });

export const put = (url, data) => instance.put(url, data);