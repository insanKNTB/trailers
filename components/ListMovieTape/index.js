import React from 'react';

import Link from "next/link";

import styles from '/styles/ListMovie/styles.module.scss'

const ListMovieTape = ({
    url = 'poster_path',
    label = 'Label',
    link = '/lists/movies',
    results,
    count
}) => {
    const resultList = results?.slice(0, count)

    return (
        <div className={styles.wrapperList}>
            <div className={styles.headerList}>
                <h4>{label}</h4>
                <Link href={link}><a>Еще</a></Link>
            </div>

            <div className={styles.wrapperListMovie}>

                {resultList?.map(item => (
                    <div
                        className={styles.listMovie}
                        key={item.id}
                    >
                        <img src={"https://image.tmdb.org/t/p/w500" + item[url]} alt=""/>
                        <p>{item.original_title || item.original_name || item.name}</p>
                    </div>
                ))}

            </div>
        </div>
    );

};

export default ListMovieTape;