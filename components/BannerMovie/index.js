import React from 'react';

import styles from '/styles/Banner/styles.module.scss'

const BannerMovie = ({
   urlBanner
}) => {
    return (
        <div className={styles.wrapperBanner}>
            <img src={urlBanner} alt=""/>
        </div>
    );
};

export default BannerMovie;