import React from 'react';

import Link from "next/link";

import Logo from 'public/vercel.svg'
import styles from '/styles/Header/header.module.scss'

const Header = () => {
    return (
        <div className={styles.wrapperHeader}>
            <div className={styles.container}>
                <div className={styles.header}>

                    <div className={styles.logo}>
                        <Link href="/">
                            <a>
                                <img src={Logo.src} alt="logo"/>
                            </a>
                        </Link>
                    </div>

                    <nav>
                        <Link href="/lists/movie"><a>Movie</a></Link>
                        <Link href="/lists/tv"><a>Series</a></Link>
                        <Link href="/lists/person"><a>Stars</a></Link>
                    </nav>

                </div>
            </div>
        </div>
    );
};

export default Header;