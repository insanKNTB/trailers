/** @type {import('next').NextConfig} */
const nextConfig = {
  reactStrictMode: true,
  swcMinify: true,
  sassOptions: {
    includePaths: ['.'],
    prependData: `@import "./styles/assets/_temp.scss";`,
  }
}

module.exports = nextConfig
