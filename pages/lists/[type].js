import React from 'react';

import useSWR from "swr";
import {getFetcher} from "/services/request";
import {useRouter} from "next/router";

import styles from '/styles/List/style.module.scss'

const Type = () => {
    const router = useRouter()
    const {query} = router

    const {data: list} = useSWR(query.type ? `/${query.type}/popular/` : null, getFetcher)

    return (
        <div className={styles.wrapper}>
            <div className={styles.container}>
                <div className={styles.wrapperListMovie}>

                    {list?.results?.map(item => (
                        <div
                            className={styles.listMovie}
                            key={item.id}
                        >
                            <img src={"https://image.tmdb.org/t/p/w500" + (item?.poster_path ? item?.poster_path : item.profile_path)} alt=""/>
                            <p>{item.original_title || item.original_name || item.name}</p>
                        </div>
                    ))}

                </div>
            </div>
        </div>
    );
};

    export default Type;