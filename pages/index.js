import ListMovieTape from "/components/ListMovieTape";
import BannerMovie from "/components/BannerMovie";

import useSWR from "swr";
import {getFetcher} from "/services/request";

import Banner from '/public/photo/banner.jpeg'
import styles from '/styles/HomePage/Home.module.scss'

const Home = () => {
    const {data: listPopularMovie} = useSWR('/movie/popular', getFetcher)
    const {data: listPopularTVShow} = useSWR('/tv/popular', getFetcher)
    const {data: listPopularPeople} = useSWR('/person/popular', getFetcher)

    return (
        <div className={styles.wrapper}>
            <BannerMovie
                urlBanner={Banner.src}
            />

            <div className={styles.container}>
                <ListMovieTape
                    count={6}
                    label="Popular movie"
                    link="/lists/movie"
                    results={listPopularMovie?.results}
                />

                <ListMovieTape
                    count={6}
                    label="TV"
                    link="/lists/tv"
                    results={listPopularTVShow?.results}
                />

                <ListMovieTape
                    count={6}
                    label="Popular"
                    url="profile_path"
                    link="/lists/person"
                    results={listPopularPeople?.results}
                />
            </div>

        </div>
    )
}

export default Home