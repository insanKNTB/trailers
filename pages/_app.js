import Layout from "/components/Layout";
import Head from "next/head";

import '/styles/reset.scss'

function MyApp({ Component, pageProps }) {
  return (
    <Layout>

        <Head>
            <title>Trailer</title>
        </Head>

        <Component {...pageProps} />
    </Layout>
  )
}

export default MyApp
